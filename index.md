Hi, thanks for considering us as partners for a project! We made you this book to explain something important.

At Candlewaster, we write only copyleft code. We ask anyone who wants to work with us to let us copyleft the work we create for them, and most of them say yes.

All other creations we make for you (like website mockups, copy, icons, photos, or drawings) are also copyleft or released into the public domain.

This is covered in the contract we gave you, but we want to make sure that you really understand how this works.

We're not lawyers, and this book isn't a legal document. However, we've made a good faith attempt to accurately portray the topic.

---

# Copyleft

Copyleft is like copyright backwards. It's not the absence of copyright (that's *public domain*), it's a type of copyright that gives others the freedom to copy as long as they share.

Copyleft says "you may do what ever you want with this work. You may use it, copy it, modify it, and even sell it, but on one condition: you must share your changed copies in the same way."

For example, Anne finds a copyleft jazz song on the internet, but it's a little boring. Anne, a musician, decides to record herself playing saxophone over the song. She has the freedom to do so, and even to sell it, but her revised song must also be copyleft. This gives others the freedom to remix her version of the song just like she remixed the original. Next, Fred can remix Anne's version, and Jen can remix Fred's version, and so on and so forth.

Code can also be copyleft. If I write a copyleft program, anyone can use and copy it. If someone improves the program, they have to share their changes, and I can choose to add their changes to my version of the program. Every day thousands of people collaborate online writing copyleft code: for websites, apps, and software.

But why do we do it? For one, we think it's the right thing to do; the more we share, the more we all have. This is hugely motivational for us and makes us love our work. When we love our work, we produce our best work.

It's also pragmatic: we build our projects with existing code and assets where possible. If those works are copyleft, our work must also be copyleft. When we use existing work to make new projects, we get a head start, and can focus on solving the important problems.

## Copyleft vs open source

Open source is also a type of copyright. Open source says "you may do what ever you want with this work. You may use it, copy it, modify it, and even sell it."

Meanwhile, copyleft says: "you may do what ever you want with this work. You may use it, copy it, modify it, and even sell it, **but on one condition: you must share your changed copies in the same way.**"

Therefore, copyleft is a variant of open source. We think the extra clause is important, because if we are sharing with people we should expect them to share back.

## How is copyleft applied?

In the US, by default, all creative work is protected by copyright. Copyright means that no one may use, copy, modify, or sell your work without your permission.

However, you can grant people a license to use your work. A license is a legal document that you distribute alongside your work, granting people permission to use the work in the way the license specifies.

You may apply copyleft to your work by distributing a copyleft license alongside your work. There are many copyleft licenses to choose from, but we have recommendations.

---

# Licensing our creations

We license different types of work under different licenses.

## Code

Code is the heart of our project, so we want anyone who copies our work to share back with us. We license all of our code under the GNU AGPL 3.0, a strong copyleft license designed for code.

As an exception, we sometimes release trivial snippets of code into the public domain using the CC0 license.

## Mockups, wireframes, style tiles

Design deliverables we create for your project are licensed under CC-BY-SA 4.0, a copyleft license for creative works.

## Diagrams, photos, illustrations, icons

When we can't find a freely-licensed asset that fits our needs, we might make our own. Copyleft licenses are rarely used for these works, so we'll follow the trend by also releasing any diagrams, photos, illustrations, and icons we create into the public domain.

Copyleft doesn't matter as much to us here because it's less likely we'll benefit from remixed versions of these works.

## Logos

Logos are much like icons, so we'll release them into the public domain using the CC0 license. This has no affect on trademark law.

---

# Transparency

Unless we publish our creations, a copyleft license doesn't do any good. Therefore, we'll put all the work we create for you online for all to see. We'll also publicly discuss the project in some places, and anyone (including outsiders) will have the opportunity to chime in. This is covered in more detail below.

We believe that the collaborative, open nature of the internet empowers people to build a better world by working together.

## GitHub

GitHub is a popular website where programmers collaboratively write open source code.

The code that powers your website will be stored in a public repository on github.com. We'll use GitHub's public issue tracker to keep track of bugs we need to fix and features we need to add, as well as discuss the goals and mission of the project at large. Anyone can read these discussions and is permitted to create a new issue or respond to an existing one.

You will have total control over the GitHub repository for your project. If you don't already have a GitHub account, we will ask you to make one. If your organization doesn't have a GitHub Organization, we'll create one for you and make you an administrator of it.

## openclipart.org

Any icons, illustrations, logos, or diagrams we create will be uploaded to a public openclipart.org account that we control. In the description box, we will write that it was created for you. Anyone has the ability to write a comment on these uploads.

## Pexels

Any photos we take for you will be uploaded to pexels.com under an account that we control.

## Candlewaster website

We might write a case study on our website about the work that we did together, including a description of the work, the challenges we overcame, and the outcome.

We might publish creations we made for you in a case study or elsewhere on our website.
